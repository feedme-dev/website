function menuSlider(){
	var topHeight = $('#menuSlider').height(); 
    var scroll = $(window).scrollTop();  
    if (scroll >= topHeight) {
	    $("#menuSlider").addClass("menuFixed");
      $("#menuSlider").removeClass("menuTop");
    }
    if (scroll < topHeight) {
      $("#menuSlider").removeClass("menuFixed");
      $("#menuSlider").addClass("menuTop");
    }
}