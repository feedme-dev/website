(function(){
	var app = angular.module('app',['angular-inview', 'smoothScroll', 'ngRoute']);

	app.config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl : 'home.html'
            })

            .when('/home', {
                templateUrl : 'home.html'
            })

            .when('/plans', {
                templateUrl : 'plans.html'
            })

            .when('/get/:type/:period', {
                templateUrl : 'get.html'
            })

            .when('/get', {
                templateUrl : 'get.html'
            })

            .when('/ways', {
                templateUrl : 'ways.html'
            })

            .when('/contact', {
                templateUrl : 'contact.html'
            })
            .otherwise('/home');
    });

	/* Scroll event to trigger animations */
	app.controller('scrollController', function($scope, $sce) {
		$scope.lineInView = function(event, type, delay){
			$(event.inViewTarget).addClass("animated");
			$(event.inViewTarget).addClass(type);
			$(event.inViewTarget).css('animation-delay', delay);
		}
	});

	app.controller('buttonController', function(){
		this.info4 = [
		    {"image":"static/images/png/shop.png", "text":"Lojas"},
		    {"image":"static/images/png/fork.png", "text":"Restaurantes"},
		    {"image":"static/images/png/drink.png", "text":"Bares"}
		]
	});

	app.controller('getAppController', function($scope, $routeParams, $http){
		$scope.sending = false;
		$scope.formData = {'type':'','name': '', 'email': '', 'section':'', 'location':'', 'store': ''};
		$scope.formData['type'] = $routeParams.type;
		$scope.formData['period'] = $routeParams.period;
		$scope.processForm = function(){
			console.log($scope.formData);
			$scope.sending = true;
			$scope.done = false;
			$http({
			  method  : 'POST',
			  url     : 'http://feedmebrasil.herokuapp.com/getapp',
			  headers: {'Content-Type': 'application/json'},
			  data: $scope.formData
			 }).success(function(data){
			 	$scope.sending = false;
				$scope.done = true;
			 });

		};
	});

	app.controller('newsletterController', function($scope, $http){
		$scope.newsletterData = {'name':'','email': ''};
		$scope.done = false;
		$scope.processNewsletter = function(){
			$scope.done = false;
			console.log($scope.newsletterData);
			$http({
			  method  : 'POST',
			  url     : 'http://feedmebrasil.herokuapp.com/newsletter',
			  headers: {'Content-Type': 'application/json'},
			  data: $scope.newsletterData
			 }).success(function(data){
			 	$scope.newsletterData = {'name':'','email': ''};
			 	$scope.done = true;
			 });

		};
	});

	app.controller('contactController', function($scope, $http){
		$scope.contactData = {'name': '', 'email': '', 'phone':'', 'msg':'', 'store': ''};
		$scope.done = false;
		$scope.processContact = function(){
			$scope.done = false;
			console.log($scope.contactData);
			$http({
			  method  : 'POST',
			  url     : 'http://feedmebrasil.herokuapp.com/contact',
			  headers: {'Content-Type': 'application/json'},
			  data: $scope.contactData
			 }).success(function(data){
			 	$scope.contactData = {'name': '', 'email': '', 'phone':'', 'msg':'', 'store': ''};
			 	$scope.done = true;
			 });

		};
	});

	app.directive('headerSite', function(){
		return{
			restrict: 'E',
			templateUrl: 'static/elements/header/header.html'
		}
	});

	app.directive('getFeedme', function(){
		return{
			restrict: 'E',
			templateUrl: 'static/elements/home/get-feedme.html'
		}
	});

	app.directive('info1', function(){
		return{
			restrict: 'E',
			templateUrl: 'static/elements/home/info-1.html'
		}
	});

	app.directive('info2', function(){
		return{
			restrict: 'E',
			templateUrl: 'static/elements/home/info-2.html'
		}
	});

	app.directive('info3', function(){
		return{
			restrict: 'E',
			templateUrl: 'static/elements/home/info-3.html'
		}
	});

	app.directive('info4', function(){
		return{
			restrict: 'E',
			templateUrl: 'static/elements/home/info-4.html'
		}
	});

	app.directive('info5', function(){
		return{
			restrict: 'E',
			templateUrl: 'static/elements/home/info-5.html'
		}
	});

	app.directive('planos', function(){
		return{
			restrict: 'E',
			templateUrl: 'static/pricing-table/plans.html'
		}
	});
	app.directive('planosJs', function(){
		return{
			restrict: 'E',
			templateUrl: 'static/pricing-table/plansJs.html'
		}
	});

	app.directive('signup', function(){
		return{
			restrict: 'E',
			templateUrl: 'static/elements/home/signup.html'
		}
	});

	app.directive('footerSite', function(){
		return{
			restrict: 'E',
			templateUrl: 'static/elements/footer/footer.html'
		}
	});

	app.directive('navbar', function(){
		return{
			restrict: 'E',
			templateUrl: 'static/elements/navbar/navbar.html'
		}
	});

	app.directive('contactMap', function(){
		return{
			restrict: 'E',
			templateUrl: 'static/elements/contact/contact-map.html'
		}
	});

	app.directive('contactForm', function(){
		return{
			restrict: 'E',
			templateUrl: 'static/elements/contact/contact-form.html'
		}
	});

	app.directive('getApp', function(){
		return{
			restrict: 'E',
			templateUrl: 'static/elements/get/get-app.html'
		}
	});

	app.directive('toten', function(){
		return{
			restrict: 'E',
			templateUrl: 'static/elements/ways/toten.html'
		}
	});

	app.directive('hotspot', function(){
		return{
			restrict: 'E',
			templateUrl: 'static/elements/ways/hotspot.html'
		}
	});

	app.directive('feedback', function(){
		return{
			restrict: 'E',
			templateUrl: 'static/elements/ways/feedback.html'
		}
	});



})();